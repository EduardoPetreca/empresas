import {
  AUTH_CLEAR,
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_SET_USER,
} from "../../constants/actionsType";

const INITIAL_STATE = {
  authenticated: false,
  errorMessage: null,
  user: null,
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case AUTH_LOGIN :
      return { ...state, authenticated: true };
    case AUTH_LOGOUT :
      return { ...state, authenticated: false, user: null, error: {} };
    case AUTH_SET_USER :
      return { ...state, user: action.payload };
    case AUTH_CLEAR :
      return {};
    default:
      return state;
  }
};
