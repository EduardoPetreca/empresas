import {
    ALERT_MESSAGE,
    AUTH_SET_USER,
    AUTH_CLEAR,
    COMPANY_CLEAR
} from "../../constants/actionsType";
import api from "../../service/api";
import {processRequestError} from "../../utils/helpers";
import AsyncStorage from "@react-native-community/async-storage";

const setHeader = (response) => {
    AsyncStorage.setItem("client", response.headers["client"]);
    AsyncStorage.setItem("token", response.headers["access-token"]);
    AsyncStorage.setItem("uid", response.headers["uid"]);
    AsyncStorage.setItem("user_name", response.data.investor.investor_name);
};

export const authLogin = (formValues, callback, fallback) => async dispatch => {
    dispatch({type: ALERT_MESSAGE, payload: null});
    await api.post("/users/auth/sign_in", formValues).then(response => {
        setHeader(response);
        dispatch({type: AUTH_SET_USER, payload: response.data});
        callback && callback(response.data);
    }).catch(reject => {
        fallback && fallback();
        processRequestError(reject, dispatch)
    });
};

export const authLogout = (callback, fallback) => async dispatch => {
    await removeItemValue(dispatch, callback, fallback);
};

async function removeItemValue(dispatch, callback, fallback) {
    try {
        dispatch({type: AUTH_CLEAR});
        dispatch({type: COMPANY_CLEAR});
        callback && callback();
        return true;
    } catch (e) {
        fallback && fallback();
        return false;
    }
}
