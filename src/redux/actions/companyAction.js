import {COMPANIES_FETCH} from '../../constants/actionsType'
import api from "../../service/api";
import {processRequestError} from "../../utils/helpers";

export const getCompanies = (callback, failBack) => async (dispatch) => {
    await api.get("/enterprises" ).then(response => {
        dispatch({type: COMPANIES_FETCH, payload: response.data.enterprises});
        callback && callback(response.data.enterprises);
    }).catch(reject => {
        failBack && failBack();
        processRequestError(reject, dispatch)
    });
};
