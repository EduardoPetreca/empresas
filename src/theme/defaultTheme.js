import { ThemePropTypes } from './propTypes';

const DefaultTheme = {
  colors: {
    background: '#FFF',
    black: '#333',
    dark: '#3C3A36',
    dark_gray: '#78746D',
    gray: '#BEBAB3',
    light_gray: '#F8F2EE',
    primary: '#E3562A',
    secondary: '#65AAEA',
    success: '#5BA092',
    error: '#EF4949',
    warning: '#F2A03F'
  },
  fontSize: {
    display: '40px',
    heading: '24px',
    paragraph: '16px',
    content: '18px',
    body: '14px',
    button: '18px',
    header : '30px',
    helper : '11px'
  },
};

DefaultTheme.propTypes = ThemePropTypes;

export default DefaultTheme;
