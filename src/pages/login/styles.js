import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import { Heading, Paragraph } from "../../components/GenericText";

export const Container = styled.View`
  flex: 1;
  padding: 16px;
  justify-content: center;
`;

export const Illustration = styled.Image`
  width: ${Dimensions.get('window').width - 50}px;
  resize-mode: stretch;
  align-self: center;
  margin-bottom: 15px;
`;

export const Title = styled(Heading)`
  text-align: center;
  font-weight: bold;
  line-height: 32px;
  letter-spacing: -0.5px;
`;

export const Subtitle = styled(Paragraph)`
  text-align: center;
  line-height: 21px;
  margin-bottom: 20px;
`;

export const BottonTitle = styled(Paragraph)`
  text-align: center;
  line-height: 21px;
  margin-top: 20px;
  margin-bottom: 20px;
`;
