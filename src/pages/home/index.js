import React, {useState, useEffect} from "react";
import { KeyboardAvoidingView, ScrollView, Platform, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {useSelector, useDispatch} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";

import {useLanguage} from "../../assets/language";
import {getCompanies} from "../../redux/actions";

import { Search, CompanyEmpty, CompanyList } from "../../components";
import { Container, Hello, Name } from "./styles";

const Home = () => {
  const {AppHome: string} = useLanguage();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  //const dataSource = useSelector(state => state.company);

  const [search, setSearch] = useState('');
  const [username, setUserName] = useState('');

  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [dataSource, setDataSource] = useState([]);

  function renderItem({ item }) {
    return (
      <CompanyList
        companies={item}
        onClick={() => navigation.navigate("Details", { company: item })}
      />
    );
  }

  function renderEmptyContent() {
    return <CompanyEmpty />;
  }

  useEffect(() => {
    dispatch(getCompanies(
      (response) => {
        setDataSource(response);
        setFilteredDataSource(response)
      },
      () => console.log('falhou')));

    getUserAsyncStorage()
  }, []);

  async function getUserAsyncStorage() {

    const user_name = await AsyncStorage.getItem("user_name");
    setUserName(user_name);

  }

  const searchFilter = (text) => {
    if (text) {
      const newData = dataSource.filter((item) => {
        const itemData = `${item.enterprise_name.toUpperCase()} ${item.enterprise_type.enterprise_type_name.toUpperCase()}`;
        const textData = text.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(dataSource);
      setSearch(text);
    }
    setSearch(text);
  };

  return (
    <>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "padding" : undefined}
        enabled>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flex: 1 }}>
          <Container>
            <Hello>{`${string.welcome}`}</Hello>
            <Name>{username}</Name>
            <Search value={search} onChangeText={text => searchFilter(text)}/>
            <FlatList
              data={filteredDataSource}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
              ListEmptyComponent={renderEmptyContent}
            />
          </Container>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default Home;
