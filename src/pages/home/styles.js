import styled from 'styled-components/native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import { Heading, Paragraph, Content, Display } from "../../components/GenericText";

export const Container = styled.View`
  flex: 1;
  padding: ${getStatusBarHeight() + 16}px 16px 16px;
`;

export const Hello = styled(Content)`
  text-align: left;
  line-height: 26px;
  letter-spacing: -0.5px;
`;

export const Name = styled(Display)`
  font-weight: bold;
  text-align: left;
  line-height: 42px;
  letter-spacing: -1px;
  
  margin-top: 10px;
`;
