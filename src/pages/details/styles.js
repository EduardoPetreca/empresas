import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import { Body, Heading } from "../../components/GenericText";
import { DefaultTheme } from "../../theme";

export const Container = styled.View`
  flex: 1;
  padding: 0 16px 16px;
`;

export const Wrapper = styled.View`
  margin: 16px 0;
`;

export const Price = styled.View`
  justify-content: center;
  align-items: center;
  align-self: flex-end;
  flex-direction: row;
  
  margin: 10px 0;
  padding: 0 10px;
  
  height: 24px;
  
  background: ${DefaultTheme.colors.secondary};
  border-radius: 12px;
`;

export const Banner = styled.Image`
  width: ${Dimensions.get('window').width}px;
  resize-mode: stretch;
  align-self: center;
  margin-bottom: 15px;
`;

export const HeaderTitle = styled.Text`
  text-align: center;
  text-transform: uppercase;
  letter-spacing: -0.5px;
  
  font-weight: bold;
  font-size: 20px;
  color: ${DefaultTheme.colors.black}
`;

export const Title = styled(Heading)`
  text-align: left;
  letter-spacing: -0.5px;
  
  font-weight: bold;
`;

export const Description = styled(Body)`
  text-align: justify;
  line-height: 21px;
  
  font-weight: bold;
  margin-top: 10px;
`;

export const PriceText = styled.Text`
  text-align: center;
  line-height: 21px;

  font-weight: bold;
  font-size: 14px;
  color: ${DefaultTheme.colors.background}
`;
