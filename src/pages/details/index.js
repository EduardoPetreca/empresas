import React, { useLayoutEffect } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";

import {useLanguage} from "../../assets/language";

import { Container, Banner, HeaderTitle, Price, PriceText, Title, Description, Wrapper } from "./styles";
import {FORMAT_PRICE} from "../../utils/format-price";
import illustration from "../../assets/images/banner.png";

const Details = () => {
  const { DetailsStrings: string } = useLanguage();

  const navigation = useNavigation();
  const route = useRoute();

  const { company } = route.params;

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => (
        <HeaderTitle>{company.enterprise_name}</HeaderTitle>
      ),
    });
  }, [navigation]);

  return (
    <Container>
      <Banner source={illustration} />
      <Price>
        <PriceText>{`${string.sharePrice} ${FORMAT_PRICE(company.share_price)}`}</PriceText>
      </Price>
      <Wrapper>
        <Title>{`${string.aboutCompany} `}</Title>
        <Description>
          {company.description}
        </Description>
      </Wrapper>

      <Wrapper>
        <Title>{`${string.moreDetails}`}</Title>
        <Description>{`${string.emailEnterprise} ${company.email_enterprise}`}</Description>
        <Description>{`${string.facebook} ${company.facebook}`}</Description>
        <Description>{`${string.twitter} ${company.twitter}`}</Description>
        <Description>{`${string.linkedin} ${company.linkedin}`}</Description>
        <Description>{`${string.phone} ${company.phone}`}</Description>
        <Description>{`${string.address} ${company.city} - ${company.country}`}</Description>
      </Wrapper>
    </Container>
  );
};
export default Details;
