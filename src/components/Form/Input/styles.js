import styled, { css } from 'styled-components/native';
import { DefaultTheme } from '../../../theme';
import { Header, Helper } from "../../GenericText";

export const Container = styled.View`
  width: 100%;
  height: 53px;
  
  padding: 0 16px;
  margin-bottom: 12px;
  
  border-radius: 12px;
  border-width: 1px;
  border-color: ${DefaultTheme.colors.gray};

  ${(props) =>
  props.isErrored &&
  css`
      border-color: ${DefaultTheme.colors.error};
      margin-bottom: 0;
    `}

  ${(props) =>
  props.isFocused &&
  css`
      border-color: ${DefaultTheme.colors.primary};
    `}
`;

export const TextInput = styled.TextInput`
  flex: 1;
  color: ${DefaultTheme.colors.dark_gray};
  font-size: 16px;
`;

export const Error = styled(Helper)`
  margin: 3px 0 10px 12px;
  
`;
