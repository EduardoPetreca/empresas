import React from 'react';
import { Wrapper, Illustration, Title, Description } from './style';

import notFound from '../../../assets/images/notFound.png'
const Companies = () => {

  return (
    <Wrapper>
      <Illustration source={notFound}/>
      <Title>Course not found</Title>
      <Description>Try searching the course with a different keyword</Description>
    </Wrapper>
  );
};

export default Companies;
