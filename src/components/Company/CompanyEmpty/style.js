import styled from 'styled-components/native';
import { Dimensions } from "react-native";
import { Heading, Paragraph } from "../../GenericText";

export const Wrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  
  margin-top: 25px;
`;

export const Illustration = styled.Image`
  width: ${Dimensions.get('window').width - 50}px;
  resize-mode: stretch;
  align-self: center;
  margin-bottom: 15px;
`;

export const Title = styled(Heading)`
  text-align: center;
  font-weight: bold;
  line-height: 32px;
  letter-spacing: -0.5px;
`;

export const Description = styled(Paragraph)`
  text-align: center;
  line-height: 21px;
  
  width: 75%;
  margin-top: 15px;
`;
