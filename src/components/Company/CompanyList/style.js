import styled from 'styled-components/native';
import { DefaultTheme } from '../../../theme';
import { Content, Paragraph } from "../../GenericText";

export const Wrapper = styled.TouchableOpacity`
  width: 100%;
  height: 95px;
  
  border-width: 1px;
  border-color: ${DefaultTheme.colors.gray};
  border-radius: 8px;
  
  margin-bottom: 16px;
  padding: 8px 16px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Column = styled.View`
  flex-direction: column;
  margin-left: 10px;
`;

export const Thumbnail = styled.Image`
  width: 78px;
  height: 78px;
  border-radius: 4px;
`;

export const CompanyName = styled(Content)`
  font-weight: bold;
  text-align: left;
  letter-spacing: -0.5px;
`;

export const CompanyType = styled(Paragraph)`
  text-align: left;
  letter-spacing: -0.5px;
  
  margin-top: 5px;
`;



