import React from 'react';
import { Container, ButtonText } from './styles';

const BackButton = ({ label, ...rest }) => (
  <Container {...rest}>
    <ButtonText>{label}</ButtonText>
  </Container>
);

export default BackButton;
